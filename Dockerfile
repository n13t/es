FROM alpine:latest
RUN apk update && apk add wget
RUN GRPC_HEALTH_PROBE_VERSION=v0.3.2 && \
    wget -qO/bin/grpc_health_probe https://github.com/grpc-ecosystem/grpc-health-probe/releases/download/${GRPC_HEALTH_PROBE_VERSION}/grpc_health_probe-linux-amd64 && \
    chmod +x /bin/grpc_health_probe

FROM scratch
COPY --from=0 /bin/grpc_health_probe /bin/grpc_health_probe
COPY eventstore /
CMD ["/eventstore", "-addr=:5000"]
