# An Event Store from [N13t](https://www.n13t.dev)
Simple event store, **append an event** & **get events** in streams

[![pipeline status](https://gitlab.com/n13t/es/badges/master/pipeline.svg)](https://gitlab.com/n13t/es/-/commits/master)
[![coverage report](https://gitlab.com/n13t/es/badges/master/coverage.svg)](https://gitlab.com/n13t/es/-/commits/master)
[![codecov](https://codecov.io/gl/n13t/es/branch/master/graph/badge.svg)](https://codecov.io/gl/n13t/es)
## Features
- [ ] Add Event/Fetch Events
- [ ] Event notification (MinIO Bucket Notification)
- [ ] Stateless
- [ ] Support Authenticate/Authorization
- [ ] RDBMS support (PostgreSQL, MySQL, MSSQL)

## Use cases
1. Use as an adapter: import event package and connect directly to backed storage
2. Run as standalone service: on other service import client package to communication

## Testing
```
go test ./... -race -coverprofile=coverage.out -covermode=atomic
go tool cover -html=coverage.out
```

## Limitations
- This Event Store is optimize for read operation
- Each stream can store maximum (2^64) events which is 18 446 744 073 709 551 615 in exact.

Default configuration limits / soft limits:
1. limit for each event payload is **1 KB**
1. fetch size is **10000 events** each time

## Notes
1. Each MinIO bucket contains multiple events streams, each event object has named with format: `<stream>_<event id>`
1. groupcache is uses in the repository for each event, and for "full-size response" in eventstore
1. Use same fetch size will increase cache hit rate
1. You can share cache by set groupcache name to the same value
