SHELL=/bin/bash -o pipefail

EXECUTABLES = docker-compose docker go
K := $(foreach exec,$(EXECUTABLES),\
        $(if $(shell which $(exec)),some string,$(error "No $(exec) in PATH")))

export GO111MODULE := on
export PATH := ${PWD}/.bin:${PATH}

.PHONY: deps
deps:
ifneq ("$(shell base64 Makefile)","$(shell cat .bin/.lock)")
		mkdir -p .bin
		curl -sfL https://install.goreleaser.com/github.com/golangci/golangci-lint.sh | sh -s -- -b .bin/ v1.27.0
		curl -sfL https://install.goreleaser.com/github.com/goreleaser/goreleaser.sh | sh -s -- -b .bin/

		go build -o .bin/mockgen github.com/golang/mock/mockgen
		go build -o .bin/swagger github.com/go-swagger/go-swagger/cmd/swagger

		echo "$$(base64 Makefile)" > .bin/.lock
endif

.PHONY: lint
lint: deps
		which golangci-lint
		GO111MODULE=on golangci-lint run -v ./...

.PHONY: cover
cover:
		go test ./... -race -coverprofile=coverage.out -covermode=atomic
		go tool cover -html=coverage.out

.PHONE: mocks
mocks: deps
		go generate ./...

.PHONY: test
test:
		go test -p 1 -count=1 -failfast ./...

.PHONY: proto-gen
proto-gen:
	./scripts/proto-gen.sh

.PHONY: mc
mc: deps
	@mc $(filter-out $@, $(MAKECMDGOALS))

.PHONY: quickstart-dev
quickstart-dev:
		docker build -f .docker/Dockerfile-build -t oryd/kratos:latest-sqlite .
		docker-compose -f quickstart.yml -f quickstart-standalone.yml up --build --force-recreate

# Formats the code
.PHONY: format
format: deps
		goreturns -w -local github.com/ory $$(listx .)
		npm run format

# Runs tests in short mode, without database adapters
.PHONY: docker
docker:
		docker build -t n13t/es:latest .

.PHONE: goreleaser
goreleaser: deps
	@goreleaser $(filter-out $@, $(MAKECMDGOALS))

%:
	@true
