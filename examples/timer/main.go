package main

import (
	"context"
	"fmt"
	"github.com/minio/minio-go/v7"
	"github.com/minio/minio-go/v7/pkg/credentials"
	"math/rand"
	"os"
	"time"

	"gitlab.com/n13t/es/pkg/event"
)

func main()  {
	var eventRepo event.Repository

	{
		endpoint := os.Getenv("MINIO_ENDPOINT")
		accessKeyID := "minio"
		secretAccessKey := "minio123"
		minioClient, err := minio.New(endpoint, &minio.Options{Creds: credentials.NewStaticV4(accessKeyID, secretAccessKey, ""), Secure: false})
		if err != nil {
			fmt.Println(err)
		}
		eventRepo = event.NewMinIOEventRepo(minioClient, "time-bucket")
	}

	// publisher publish time randomly
	go func()  {
		var processedEventId uint64
		for {
			now := time.Now()

			// fetch events
			processedEventId++

			// publish time
			opts := event.AddEventOptions{
				ContentType: "plain/text",
			}
			eventRepo.AddEvent(context.Background(), "text-time", now.Local().String(), opts, processedEventId)

			// wait [0-5000] (ms) to add new event
			time.Sleep(time.Duration(rand.Intn(5000)) * time.Millisecond)
		}
	}()

	// consumer that calculate average duration
	{
		var (
			processedEventID uint64
			counter int64
			avg time.Duration
			previousTime time.Time
		)

		for {
			cb := func (ev event.Event)  {
				currentTime := time.Now()
				d := currentTime.Sub(previousTime)
				avg = time.Duration(int64((avg * time.Duration(counter)) + d) / (counter + 1))
				counter++
				processedEventID++

			}

			eventRepo.OnEventAdded(context.Background(), "text-time", cb)


		}
	}
}
