package event

import (
	"context"
	"database/sql"
	"fmt"
	"gitlab.com/n13t/es/pkg/errors"
	"regexp"
	"strings"
)

type repo struct {
	driver string
	db *sql.DB
}

func NewSQLEventRepository(dataSource string) Repository {
	var (
		db *sql.DB
		err error
		driver = strings.Split(dataSource, "://")[0]
	)

	switch driver {
	case "postgres", "mysql", "mssql":
		db, err = sql.Open(driver, dataSource)
		if err != nil {
			panic(err)
		}
	default:
		panic(errors.ErrUnsupportedDriver)
	}

	return &repo{
		driver: driver,
		db: db,
	}
}

// this query will be used to create event table in PostgreSQL, MySQL, MSSQL
const genericCreateEventsTableQuery = `CREATE TABLE %s(id BIGINT NOT NULL, data TEXT, meta TEXT, CONSTRAINT %s_pkey PRIMARY KEY (id))`

// template for event insert query with optimistic control
// this query MAY NOT safe be used directly, use the specify version for each database instead
const insertEventBaseQuery = `
	INSERT INTO %s(id, meta, data)
	SELECT %s, %s, %s
	WHERE (SELECT max(id) FROM %s) = %s
	   OR (SELECT count(1) FROM %s) = 0
`
//const postgresInsertEventQuery = `
//	INSERT INTO %s(id, meta, data)
//	SELECT $3 + 1, $1, $2
//	WHERE (SELECT max(id) FROM %s) = $3
//		OR (SELECT count(1) FROM %s) = 0
//`
var postgresInsertEventQuery = fmt.Sprintf(insertEventBaseQuery, "%s", "$3+1", "$1", "$2", "%s", "$3", "%s")

//const mysqlInsertEventQuery = `
//	INSERT INTO %s(id, meta, data)
//	SELECT ?, ?, ?
//	WHERE (SELECT max(id) FROM %s) = ?
//	   OR (SELECT count(1) FROM %s) = 0
//	`
var mysqlInsertEventQuery = fmt.Sprintf(insertEventBaseQuery, "%s", "?", "?", "?", "%s", "?", "%s")

//const mssqlInsertEventQuery = `
//	INSERT INTO %s(id, meta, data)
//	SELECT @p3 + 1, @p1, @p2
//	WHERE (SELECT max(id) FROM %s) = @p3
//	   OR (SELECT count(1) FROM %s) = 0
//	`
var mssqlInsertEventQuery = fmt.Sprintf(insertEventBaseQuery, "%s", "@p3+1", "@p1", "@p2", "%s", "@p3", "%s")

const genericSelectEvents = `SELECT id, meta, data FROM %s WHERE id > $1 ORDER BY id LIMIT $2`

func (r *repo) AddEvent(ctx context.Context, Stream string, Data string, Opts AddEventOptions, LatestEventId uint64) error {
	var (
		result sql.Result
		err error
		query string
		args []interface{}
	)

	switch r.driver {
	case "postgres": {
		query = postgresInsertEventQuery
		args = []interface{}{"meta", Data, LatestEventId}
	}
	case "mysql": {
		query = mysqlInsertEventQuery
		args = []interface{}{LatestEventId + 1, "meta", Data, LatestEventId}
	}
	case "sqlserver": {
		query = mssqlInsertEventQuery
		args = []interface{}{"meta", Data, LatestEventId}
	}

	}

	// exec query
	result, err = r.db.ExecContext(context.Background(), fmt.Sprintf(query, Stream, Stream, Stream), args...)
	if err != nil {
		// if error table not exists then create one
		fmt.Println(regexp.MatchString(`.*exist.*`, err.Error()))
		if matched, err := regexp.MatchString(`.*exist.*`, err.Error()); err == nil && matched {
			fmt.Println(fmt.Sprintf(genericCreateEventsTableQuery, Stream, Stream))
			if _, err := r.db.ExecContext(context.Background(), fmt.Sprintf(genericCreateEventsTableQuery, Stream, Stream)); err != nil {
				return err
			}

			// try query
			result, err = r.db.ExecContext(context.Background(), fmt.Sprintf(query, Stream, Stream, Stream), args...)
		} else {
			return err
		}
	}

	a, err := result.RowsAffected()
	if err != nil {
		return err
	} else if a != 1 {
		return errors.ErrAddEventFailed
	}

	return nil
}

func (r *repo) GetEvent(ctx context.Context, Stream string, ID uint64) (*Event, error) {
	panic(errors.ErrUnimplemented)

	return nil, nil
}

func (r *repo) OnEventAdded(ctx context.Context, Stream string, Callback func(Event)) {
	panic(errors.ErrUnimplemented)

	return
}
