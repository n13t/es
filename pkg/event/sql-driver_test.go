package event

import (
	"context"
	"database/sql"
	"fmt"
	_ "github.com/denisenkom/go-mssqldb"
	_ "github.com/go-sql-driver/mysql"
	_ "github.com/lib/pq"
	"gitlab.com/n13t/es/pkg/errors"
	"os"
	"os/exec"
	"testing"
	"time"
)

func startDockerCompose(_ *testing.T) {
	dir, _ := os.Getwd()
	_ = exec.Command("docker-compose", "-f", dir+"/testdata/docker-compose.yml", "kill").Start()
	_ = exec.Command("docker-compose", "-f", dir+"/testdata/docker-compose.yml", "rm", "-f").Start()
	err := exec.Command("docker-compose", "-f", dir+"/testdata/docker-compose.yml", "up", "-d").Start()
	if err != nil {
		panic(err)
	}
}

func killDockerCompose(_ *testing.T) {
	dir, _ := os.Getwd()
	err := exec.Command("docker-compose", "-f", dir+"/testdata/docker-compose.yml", "kill").Start()
	if err != nil {
		panic(err)
	}
}

func TestSQLDrivers(t *testing.T) {
	t.Skip()

	startDockerCompose(t)
	defer killDockerCompose(t)

	fmt.Println("Wait 15s for RDBMS to be up and running")
	time.Sleep(15 * time.Second)

	cfgs := []struct{
		name, driver, dataSource string
	}{
		{name: "postgres:12.3", driver: "postgres", dataSource: "postgres://postgres:postgres@localhost:15432/postgres?sslmode=disable"},
		{name: "mariadb:10.4", driver: "mysql", dataSource: "root:mypasswd@tcp(localhost:13306)/maria"},
		{name: "mssql:2019", driver: "sqlserver", dataSource: "sqlserver://sa:myStrong!Passwd@localhost:11433?database=master"},
	}

	stream := "testdata"

	for _, cfg := range cfgs {
		t.Run(cfg.name, func(t *testing.T) {
			db, err := sql.Open(cfg.driver, cfg.dataSource)
			if err != nil {
				t.Fatal("Error creating connection pool: ", err.Error())
			}

			switch cfg.driver {
			case "postgres":  {
				result, err := db.ExecContext(context.Background(), fmt.Sprintf(genericCreateEventsTableQuery, stream, stream))
				fmt.Println(result, err)

				fmt.Println(db.ExecContext(context.Background(), fmt.Sprintf(postgresInsertEventQuery, stream, stream, stream), "meta", "data", 0))
				fmt.Println(db.ExecContext(context.Background(), fmt.Sprintf(postgresInsertEventQuery, stream, stream, stream), "meta", "data", 1))
				fmt.Println(db.ExecContext(context.Background(), fmt.Sprintf(postgresInsertEventQuery, stream, stream, stream), "meta", "data", 2))
			}
			case "mysql": {
				result, err := db.ExecContext(context.Background(), fmt.Sprintf(genericCreateEventsTableQuery, stream, stream))
				fmt.Println(result, err)

				fmt.Println(db.ExecContext(context.Background(), fmt.Sprintf(mysqlInsertEventQuery, stream, stream, stream), 1, "meta", "data", 0))
				fmt.Println(db.ExecContext(context.Background(), fmt.Sprintf(mysqlInsertEventQuery, stream, stream, stream), 2, "meta", "data", 1))
				fmt.Println(db.ExecContext(context.Background(), fmt.Sprintf(mysqlInsertEventQuery, stream, stream, stream), 3, "meta", "data", 2))
			}
			case "sqlserver": {
				result, err := db.ExecContext(context.Background(), fmt.Sprintf(genericCreateEventsTableQuery, stream, stream))
				fmt.Println(result, err)

				fmt.Println(db.ExecContext(context.Background(), fmt.Sprintf(mssqlInsertEventQuery, stream, stream, stream), "meta", "data", 0))
				fmt.Println(db.ExecContext(context.Background(), fmt.Sprintf(mssqlInsertEventQuery, stream, stream, stream), "meta", "data", 1))
				fmt.Println(db.ExecContext(context.Background(), fmt.Sprintf(mssqlInsertEventQuery, stream, stream, stream), "meta", "data", 2))
			}
			default:
				t.Error(errors.ErrUnsupportedDriver)
			}


		})
	}
}



func insertEvent(t *testing.T) {
	table := "testdata"
	id := uint64(0)
	limit := uint64(1000)
	query := fmt.Sprintf(`INSERT INTO %s(id, data, meta) SET  WHERE id > %d LIMIT %d`, table, id, limit)

	var db *sql.DB
	rows, err := db.QueryContext(context.Background(), query)
	if err != nil {
		t.Error(err)
		return
	}

	fmt.Println(rows)
}

func queryEvents(t *testing.T) {
	table := "table"
	id := uint64(0)
	limit := uint64(1000)
	query := fmt.Sprintf("SELECT * FROM %s WHERE id > %d LIMIT %d", table, id, limit)

	var db *sql.DB
	rows, err := db.QueryContext(context.Background(), query)
	if err != nil {
		t.Error(err)
		return
	}

	fmt.Println(rows)
}

