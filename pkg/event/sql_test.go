package event

import (
	"context"
	"fmt"
	"testing"
)

func TestRepo_AddEvent(t *testing.T) {
	dataSource := "postgres://postgres:postgres@localhost:5432/postgres?sslmode=disable"
	repo := NewSQLEventRepository(dataSource)

	fmt.Println(repo.AddEvent(context.Background(), "my_stream", "data", AddEventOptions{}, 0)) // success
	fmt.Println(repo.AddEvent(context.Background(), "my_stream", "data", AddEventOptions{}, 0)) // fail
	fmt.Println(repo.AddEvent(context.Background(), "my_stream", "data", AddEventOptions{}, 1)) // success
}
