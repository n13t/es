package event

import (
	"context"
	"fmt"
	"github.com/minio/minio-go/v7/pkg/credentials"
	"log"
	"os"
	"strings"
	"testing"
	"time"

	"github.com/minio/minio-go/v7"
)

var (
	testMinIOClient   *minio.Client
	testBucketName    = "testdata" // all test event/data will be store in a single MinIO bucket
	testMinIOTestRepo Repository
)

func init() {
	if os.Getenv(minioEndpoint) == "" {
		return
	}

	var err error
	endpoint := os.Getenv(minioEndpoint)
	//accessKeyID := "minio"
	//secretAccessKey := "minio123"

	cred := credentials.NewEnvMinio()
	testMinIOClient, err = minio.New(endpoint, &minio.Options{
		Creds: cred, Secure: false,
	})
	if err != nil {
		log.Fatalln(err)
	}

	createBucketIfNotExists(testMinIOClient, testBucketName)

	// create bucket & testdata
	//err = testMinIOClient.MakeBucket(context.Background(), testBucketName, minio.MakeBucketOptions{ObjectLocking: true, Region: ""})
	//if err != nil {
	//	// check if bucket exists
	//	exists, errBucketExists := testMinIOClient.BucketExists(context.Background(), testBucketName)
	//	if errBucketExists != nil {
	//		panic(errBucketExists)
	//	} else if exists {
	//		// testdata bucket exists
	//		fmt.Printf("bucket %s exists\n", testBucketName)
	//	}
	//} else {
		fmt.Printf("Generate testdata... ")
		bucketEventGenerator(testMinIOClient, testBucketName, "test", 100) // generate 100 events
		fmt.Printf("done.\n")
	//}

	{
		testMinIOTestRepo = NewMinIOEventRepo(testMinIOClient, testBucketName)
	}
}

func TestMinIO(t *testing.T) {
	if testMinIOClient == nil {
		t.Skip(fmt.Sprintf("test(minio): skipping\n%s not defined", minioEndpoint))
	}

	//var err error

	/**
	 * Make bucket
	 */
	//location := "test-location"
	//ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	//defer cancel()

	//err = testMinIOClient.MakeBucketWithObjectLockWithContext(ctx, testBucketName, location)
	//if err != nil {
	//	// Check to see if we already own this bucket (which happens if you run this twice)
	//	exists, errBucketExists := testMinIOClient.BucketExists(testBucketName)
	//	if errBucketExists == nil && exists {
	//		log.Printf("We already own %s\n", testBucketName)
	//	} else {
	//		log.Fatalln(err)
	//	}
	//} else {
	//	log.Printf("Successfully created %s\n", testBucketName)
	//}

	// setup Locking
	//{
	//	mode := minio.Compliance
	//	validity := uint(1)
	//	validityUnit := minio.Years
	//	if err := client.SetBucketObjectLockConfig(testBucketName, &mode, &validity, &validityUnit); err != nil {
	//		panic(err)
	//	}
	//}

	createBucketIfNotExists(testMinIOClient, testBucketName)

	processedEventID := uint64(0)
	text := `{"key": "value"}`
	stream := "my-stream"

	for i := 0; i < 100; i++ {
		for {
			ev, err := testMinIOTestRepo.GetEvent(context.Background(), stream, processedEventID + 1)
			if err != nil {
				break
			} else {
				if ev != nil {
					processedEventID = ev.ID
				} else {
					break
				}
			}
		}

		err := testMinIOTestRepo.AddEvent(context.Background(), stream, text, AddEventOptions{
			ContentType: "application/vnd.org.domain-event+json",
		}, processedEventID)
		if err != nil {
			t.Error(err)
		}
	}
}

// Verify that we can prevent data write race condition without using retention
func TestMinIOPreventOverwrite(t *testing.T) {
	if testMinIOClient == nil {
		t.Skip(fmt.Sprintf("test(minio): skipping\n%s not defined", minioEndpoint))
	}

	testObjectName := "overwrite"
	original := "my original-data"
	temped := "data that should not be write"
	opts := minio.PutObjectOptions{
		Mode: minio.Compliance,
		RetainUntilDate: time.Now().AddDate(1, 0,0),
		LegalHold: minio.LegalHoldEnabled,
	}
	{
		info, err := testMinIOClient.PutObject(context.Background(), testBucketName, testObjectName, strings.NewReader(original), int64(len(original)), opts)
		if err != nil {
			t.Error(err)
		} else {
			t.Logf("%+v", info)
		}
	}

	{
		info, err := testMinIOClient.PutObject(context.Background(), testBucketName, testObjectName, strings.NewReader(temped), int64(len(temped)), minio.PutObjectOptions{

		})
		if err != nil {
			t.Error(err)
		} else {
			t.Logf("%+v", info)
		}
	}
}

// Verify MinIO can prevent delete event object
func TestEventImmutability(t *testing.T) {
	if testMinIOClient == nil {
		t.Skip(fmt.Sprintf("test(minio): skipping\n%s not defined", minioEndpoint))
	}

	const testStream = "immutability"

	repo := NewMinIOEventRepo(testMinIOClient, testBucketName)
	if err := repo.AddEvent(context.Background(), testStream, "my-payload", AddEventOptions{}, 0); err != nil {
		t.Error(err)
	}

	if err := testMinIOClient.RemoveObject(context.Background(), testBucketName, buildObjectKey(testStream, 1), minio.RemoveObjectOptions{}); err == nil {
		fmt.Printf("%s", "expected RemoveObject to fail")
		//t.Errorf("%s", "expected RemoveObject to fail")
	}
}

func BenchmarkMinioEventRepo_Fetch10kEvents(b *testing.B) {
	if testMinIOClient == nil {
		b.Skip(fmt.Sprintf("test(minio): skipping\n%s not defined", minioEndpoint))
		return
	}

	var (
		stream = "test"
	)

	//_ = testMinIOClient.MakeBucketWithObjectLockWithContext(context.Background(), testBucketName, "")

	benchmarks := []struct {
		fetchSize int64
	}{
		{10},
		{10},
		{50},
		{100},
		{500},
		{1000},
		{5000},
		{10000}, // max
	}

	for _, bm := range benchmarks {
		b.Run(fmt.Sprintf("Size %d", bm.fetchSize), func(b *testing.B) {
			for i := 0; i < b.N; i++ {
				var processedEventID = uint64(0)
				for {
					if ev, err := testMinIOTestRepo.GetEvent(context.Background(), stream, processedEventID); err != nil {
						b.Error(err)
					} else {
						// l := len(events)
						// if l == 0 {
						// 	break
						// }
						if ev == nil {
							break
						}
						processedEventID = ev.ID
					}

					time.Sleep(5 * time.Millisecond) // simulate network latency
				}
			}
		})
	}
}

// This test will create 2 stream, 1 for containt "source" events
// other will mirrong those events using OnEventAdded listener
func TestMirroringWOnEventAddedListener(t *testing.T) {
	if testMinIOClient == nil {
		t.Skip(fmt.Sprintf("test(minio): skipping\n%s not defined", minioEndpoint))
	}

	const (
		minioBucket = "testdata"
		srcStream   = "src"
		dstStream   = "dst"
	)


	createBucketIfNotExists(testMinIOClient, minioBucket)

	go func() {
		processedEventID := uint64(0)
		c := uint64(1)

		testMinIOTestRepo.OnEventAdded(context.Background(), srcStream, func(ev Event) {
			for {
				de, err := testMinIOTestRepo.GetEvent(context.Background(), dstStream, c)
				if err != nil || de == nil {
					break
				} else {
					processedEventID = de.ID
					c++
				}
			}
			// AddEvent
			_ = testMinIOTestRepo.AddEvent(context.Background(), dstStream, ev.Data, AddEventOptions{
				ContentType: ev.Type,
			}, processedEventID)
		})
	}()

	bucketEventGenerator(testMinIOClient, minioBucket, srcStream, 100)
	time.Sleep(5 * time.Second)
}

//func createBucketIfNotExists(Name string) {
//	ctx, cancel := context.WithTimeout(context.Background(), 3*time.Second)
//	defer cancel()
//
//	err := testMinIOClient.MakeBucket(ctx, Name, minio.MakeBucketOptions{ObjectLocking: true})
//	if err != nil {
//		// Check to see if we already own this bucket (which happens if you run this twice)
//		exists, errBucketExists := testMinIOClient.BucketExists(context.Background(), Name)
//		if errBucketExists == nil && exists {
//			log.Printf("We already own %s\n", Name)
//		} else {
//			log.Fatalln(err)
//		}
//	} else {
//		log.Printf("Successfully created %s\n", Name)
//	}
//}

func bucketEventGenerator(testMinIOClient *minio.Client, testBucketName string, Stream string, size uint64) {
	data := make([]byte, 1000)
	reader := strings.NewReader(string(data))

	for i := uint64(1); i < size+1; i++ {
		_, err := testMinIOClient.PutObject(context.Background(), testBucketName, buildObjectKey(Stream, i), reader, reader.Size(), minio.PutObjectOptions{
			DisableMultipart: true,
			ContentType:      "application/vnd.org.domain-event+text",
		})
		if err != nil {
			panic(err)
		}
	}

	// make 999 duplicates
	//src := minio.NewSourceInfo(testBucketName, fmt.Sprintf(eventObjectNameFormat, Stream, 1), nil)
	//
	//for i := 2; int64(i) < size + 1; i++ {
	//	dst, err := minio.NewDestinationInfo(testBucketName, fmt.Sprintf(eventObjectNameFormat, Stream, i), nil, nil)
	//	if err != nil {
	//		fmt.Println(err)
	//	} else {
	//		// Copy object call
	//		err = testMinIOClient.CopyObject(dst, src)
	//		if err != nil {
	//			fmt.Println(err)
	//			return
	//		}
	//	}
	//}
}
