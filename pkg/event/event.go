package event

// Event ...
type Event struct {
	ID     uint64 `json:"id"`
	Stream string `json:"stream"`
	Type   string `json:"type"`
	Data   string `json:"data"`
}
