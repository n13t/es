//go:generate mockgen -source=$GOFILE -destination=mocks/$GOFILE

package event

import (
	"context"
	"time"
)

type Repository interface {
	AddEvent(ctx context.Context, Stream string, Payload string, Opts AddEventOptions, LatestEventId uint64) error
	GetEvent(ctx context.Context, Stream string, ID uint64) (*Event, error)
	OnEventAdded(ctx context.Context, Stream string, Callback func(Event))
}

// AddEventOptions is subset of minio.PutObjectOptions
type AddEventOptions struct {
	ContentType        string
	ContentEncoding    string
	ContentDisposition string
	ContentLanguage    string
	Tags               map[string]string
	RetainUntilDate    *time.Time
	StorageClass       string
	SendContentMd5     bool
}
