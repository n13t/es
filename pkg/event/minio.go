// MinIO implementation of Event Repository

package event

import (
	"bufio"
	"context"
	"errors"
	"fmt"
	"strconv"
	"strings"
	"time"

	"github.com/golang/groupcache"
	"github.com/minio/minio-go/v7"
	//"path"
	//"path/filepath"
)

const (
	minioEventObjectNameFormat = "%s/%d" // <stream>_<id> to support multiples stream per bucket
	minioEndpoint              = "MINIO_ENDPOINT"
)

type minioEventRepo struct {
	client       *minio.Client
	bucket       string
	mode         minio.RetentionMode
	validity     uint
	validityUnit minio.ValidityUnit

	eventSizeLimit int64

	// optional
	grpCache         *groupcache.Group
	cacheSize        int64
	cacheHitCounter  int64
	cacheMissCounter int64
}



func (m *minioEventRepo) AddEvent(ctx context.Context, Stream string, PayLoad string, Opts AddEventOptions, LatestEventID uint64) error {
	if len(PayLoad) > int(m.eventSizeLimit) {
		return fmt.Errorf("add(limit): event size cannot bigger than %d bytes", m.eventSizeLimit)
	}

	// check if lastest event id exists
	if LatestEventID > 0 {
		latestMinIOObjectKey := buildObjectKey(Stream, LatestEventID)

		if objInfo, err := m.client.StatObject(ctx, m.bucket, latestMinIOObjectKey, minio.StatObjectOptions{}); err != nil {
			return err
		} else {
			if objInfo.Key != latestMinIOObjectKey {
				return errors.New("add(validate): latest event id not found")
			}
		}
	}

	// check if new event id is vailable
	newEventID := LatestEventID + 1
	newEventObjectName := fmt.Sprintf(minioEventObjectNameFormat, Stream, newEventID)
	if _, err := m.client.StatObject(context.Background(), m.bucket, newEventObjectName, minio.StatObjectOptions{}); err != nil {
		errResponse := minio.ToErrorResponse(err)
		if errResponse.Code == "NoSuchKey" {
			// continue
		} else {
			return err
		}
	} else {
		return errors.New("add(validate): event exists")
	}

	// prepare
	//var retainUntilDate time.Time
	//{
	//	if Opts.RetainUntilDate != nil {
	//		retainUntilDate = *Opts.RetainUntilDate
	//	} else {
	//		retainUntilDate = time.Now().AddDate(1, 0, 0) // default retain is 1 year
	//	}
	//}
	//retainUntilDate := time.Now().AddDate(1, 0, 0) // default retain is 1 year
	opts := minio.PutObjectOptions{
		Mode: minio.Compliance,
		RetainUntilDate: time.Now().AddDate(1, 0, 0),
		//Mode:              	&m.mode,
		//RetainUntilDate:  	&retainUntilDate,
		DisableMultipart: 	true,
		ContentType:      	Opts.ContentType,
		//ContentEncoding:  	Opts.ContentEncoding,
		//ContentLanguage:  	Opts.ContentLanguage,
		//ContentDisposition: 	Opts.ContentDisposition,
		//UserTags:         	Opts.Tags,
		//SendContentMd5:   	true,
		LegalHold: minio.LegalHoldEnabled,


	}
	//opts.Header().Set("Content-Type", Meta["Content-Type"])
	//fmt.Printf("%+v", opts)
	_ = opts

	reader := strings.NewReader(PayLoad)
	_, err := m.client.PutObject(context.Background(), m.bucket, newEventObjectName, reader, reader.Size(), minio.PutObjectOptions{
		ContentType: Opts.ContentType,
	})
	return err
}

//func (m *minioEventRepo) FetchEvents(ctx context.Context, Stream string, Current, Next int64) ([]*Event, error) {
//	var (
//		events = make([]Event, 0)
//	)
//
//	for i := Current + 1; i <= Current + Next; i++ {
//		event, err := m.fetchById(ctx, Stream, i, true)
//		if err != nil {
//			return nil, err
//		}
//
//		if event == nil {
//			break
//		}
//
//		events = append(events, *event)
//	}
//
//	return events, nil
//}

func (m *minioEventRepo) GetEvent(ctx context.Context, Stream string, ID uint64) (*Event, error) {
	var (
		eventKeyName = fmt.Sprintf(minioEventObjectNameFormat, Stream, ID)
		data         string
		objectInfo   minio.ObjectInfo
	)

	obj, err := m.client.GetObject(context.Background(), m.bucket, eventKeyName, minio.GetObjectOptions{})
	if err != nil {
		return nil, err
	} else if objectInfo, _ = obj.Stat(); objectInfo.Err != nil || objectInfo.Size == 0 {
		return nil, nil
	}

	scanner := bufio.NewScanner(obj)
	for scanner.Scan() {
		data = scanner.Text()
		break
	}

	return &Event{
		ID:   ID,
		Type: objectInfo.ContentType,
		Data: data,
	}, nil
}

func (m *minioEventRepo) OnEventAdded(_ context.Context, stream string, callback func(event Event)) {
	// Create a done channel to control 'ListenBucketNotification' go routine.
	doneCh := make(chan struct{})

	// Indicate a background go-routine to exit cleanly upon return.
	defer close(doneCh)

	// Listen for bucket notifications on "mybucket" filtered by prefix, suffix and events.
	// FIXME: update to v7
	//for notificationInfo := range m.client.ListenBucketNotification(context.Background(), m.bucket, fmt.Sprintf("%s", stream), "", []string{"s3:ObjectCreated:*"}, doneCh) {
	//	if notificationInfo.Err != nil {
	//		fmt.Println(notificationInfo.Err)
	//	} else {
	//
	//		for _, notificationEvent := range notificationInfo.Records {
	//			// fmt.Printf("%+v\n", notificationEvent.S3.Object)
	//			id, _ := objectKey2ID(stream, notificationEvent.S3.Object.Key)
	//			ev, _ := m.GetEvent(context.Background(), stream, id)
	//			if ev != nil {
	//				callback(*ev)
	//			}
	//		}
	//
	//	}
	//}
}

func (m *minioEventRepo) getById(ctx context.Context, Stream string, EventID uint64, UseCache bool) (*Event, error) {
	var (
		eventKeyName = fmt.Sprintf(minioEventObjectNameFormat, Stream, EventID)
		payload      []byte
	)

	if UseCache {
		err := m.grpCache.Get(ctx, eventKeyName, groupcache.AllocatingByteSliceSink(&payload))
		if err == nil {
			m.cacheHitCounter++
			return &Event{
				ID:   EventID,
				Data: string(payload),
			}, nil
		}
		m.cacheMissCounter++
		// cache missed
		//fmt.Printf("fetch(cache): %v\n", err)
	}

	obj, err := m.client.GetObject(context.Background(), m.bucket, eventKeyName, minio.GetObjectOptions{})
	if err != nil {
		return nil, err
	} else if stat, _ := obj.Stat(); stat.Err != nil || stat.Size == 0 {
		return nil, nil
	}

	scanner := bufio.NewScanner(obj)
	for scanner.Scan() {
		payload = []byte(scanner.Text())
		break
	}

	return &Event{
		ID:   EventID,
		Data: string(payload),
	}, nil
}

func objectKey2ID(Stream string, Key string) (id uint64, err error) {
	s := strings.Split(Key, fmt.Sprintf("%s/", Stream))
	if len(s) != 2 {
		panic("fatal(event): unexpected event format")
	}

	return strconv.ParseUint(s[1], 10, 64)
}

func buildObjectKey(Stream string, ID uint64) (ObjectKey string) {
	return fmt.Sprintf(minioEventObjectNameFormat, Stream, ID)
}

func NewMinIOEventRepo(Client *minio.Client, Bucket string) Repository {
	groupCacheName := fmt.Sprintf("%s_events", Bucket)

	createBucketIfNotExists(Client, Bucket)

	mode := minio.Compliance
	validity := uint(1)
	unit := minio.Years

	err := Client.SetObjectLockConfig(context.Background(), Bucket, &mode, &validity, &unit)
	if err != nil {
		panic(err)
	}

	//Client.Set

	//fmt.Printf("%+v\n", Client.GetObjectLockConfig(Bucket))

	repo := minioEventRepo{
		client:         Client,
		bucket:         Bucket,
		mode:           minio.Compliance,
		validity:       uint(1),
		validityUnit:   minio.Years,
		eventSizeLimit: 1000, // bytes
		grpCache: groupcache.GetGroup(groupCacheName),
	}

	if repo.grpCache == nil {
		repo.cacheSize = 100000 * repo.eventSizeLimit // 100k full events * 1KB/event = 100 MB
		repo.grpCache = groupcache.NewGroup(groupCacheName, repo.cacheSize, groupcache.GetterFunc(
			func(ctx context.Context, key string, dest groupcache.Sink) error {
				//log.Println("looking up", key)
				s := strings.Split(key, "/")
				// eventID, err := strconv.ParseInt(s[1], 10, 64)
				eventID, err := objectKey2ID(s[0], key)
				if err != nil {
					return err
				}

				event, err := repo.getById(ctx, s[0], eventID, false)
				if err != nil {
					return err
				}

				if event == nil {
					return errors.New("not found")
				}

				_ = dest.SetBytes([]byte(event.Data))
				return nil
			},
		))
	}

	return &repo
}

func createBucketIfNotExists(Client *minio.Client, Name string) {
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()

	err := Client.MakeBucket(ctx, Name, minio.MakeBucketOptions{ObjectLocking: true})
	if err != nil {
		// Check to see if we already own this bucket (which happens if you run this twice)
		exists, errBucketExists := Client.BucketExists(context.Background(), Name)
		if errBucketExists == nil && exists {
			fmt.Printf("We already own %s\n", Name)
		} else {
			fmt.Errorf("%v", err)
		}
	} else {
		fmt.Printf("Successfully created %s\n", Name)
	}
}
