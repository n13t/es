package transport

import (
	"bytes"
	"context"
	"encoding/json"
	"errors"
	"io/ioutil"
	"net/http"
	"net/url"
	"strconv"
	"strings"
	"time"

	"golang.org/x/time/rate"

	"github.com/go-kit/kit/circuitbreaker"
	"github.com/go-kit/kit/endpoint"
	"github.com/go-kit/kit/log"
	"github.com/go-kit/kit/ratelimit"
	"github.com/go-kit/kit/tracing/opentracing"
	"github.com/go-kit/kit/tracing/zipkin"
	"github.com/go-kit/kit/transport"
	httptransport "github.com/go-kit/kit/transport/http"
	stdopentracing "github.com/opentracing/opentracing-go"
	stdzipkin "github.com/openzipkin/zipkin-go"
	"github.com/sony/gobreaker"

	esendpoint "gitlab.com/n13t/es/pkg/eventstore/endpoint"
	"gitlab.com/n13t/es/pkg/eventstore/service"
)

func NewHTTPHandler(endpoints esendpoint.Set, otTracer stdopentracing.Tracer, zipkinTracer *stdzipkin.Tracer, logger log.Logger) http.Handler {
	options := []httptransport.ServerOption{
		httptransport.ServerErrorEncoder(errorEncoder),
		httptransport.ServerErrorHandler(transport.NewLogErrorHandler(logger)),
	}

	if zipkinTracer != nil {
		// Zipkin HTTP Server Trace can either be instantiated per endpoint with a
		// provided operation name or a global tracing service can be instantiated
		// without an operation name and fed to each Go kit endpoint as ServerOption.
		// In the latter case, the operation name will be the endpoint's http method.
		// We demonstrate a global tracing service here.
		options = append(options, zipkin.HTTPServerTrace(zipkinTracer))
	}

	addEventHandler := httptransport.NewServer(
		endpoints.AddEventEndpoint,
		decodeHTTPAddEventRequest,
		encodeHTTPGenericResponse,
		append(options, httptransport.ServerBefore(opentracing.HTTPToContext(otTracer, "AddEvent", logger)))...,
	)
	fetchEventsHandler := httptransport.NewServer(
		endpoints.FetchEventsEndpoint,
		decodeHTTPFetchEventsRequest,
		//encodeHTTPGenericResponse,
		encodeHTTPFetchEventsResponse,
		append(options, httptransport.ServerBefore(opentracing.HTTPToContext(otTracer, "FetchEvents", logger)))...,
	)

	m := http.NewServeMux()
	m.HandleFunc("/events", func(writer http.ResponseWriter, request *http.Request) {
		switch request.Method {
		case http.MethodPost:
			addEventHandler.ServeHTTP(writer, request)
		case http.MethodGet:
			fetchEventsHandler.ServeHTTP(writer, request)
		default:
			http.Error(writer, "Method not allowed", http.StatusMethodNotAllowed)
		}
	})

	return m
}

// NewHTTPClient returns an AddService backed by an HTTP server living at the
// remote instance. We expect instance to come from a service discovery system,
// so likely of the form "host:port". We bake-in certain middlewares,
// implementing the client library pattern.
func NewHTTPClient(instance string, otTracer stdopentracing.Tracer, zipkinTracer *stdzipkin.Tracer, logger log.Logger) (service.Service, error) {
	// Quickly sanitize the instance string.
	if !strings.HasPrefix(instance, "http") {
		instance = "http://" + instance
	}
	u, err := url.Parse(instance)
	if err != nil {
		return nil, err
	}

	// We construct a single ratelimiter middleware, to limit the total outgoing
	// QPS from this client to all methods on the remote instance. We also
	// construct per-endpoint circuitbreaker middlewares to demonstrate how
	// that's done, although they could easily be combined into a single breaker
	// for the entire remote instance, too.
	limiter := ratelimit.NewErroringLimiter(rate.NewLimiter(rate.Every(time.Second), 100))

	// global client middlewares
	var options []httptransport.ClientOption

	if zipkinTracer != nil {
		// Zipkin HTTP Client Trace can either be instantiated per endpoint with a
		// provided operation name or a global tracing client can be instantiated
		// without an operation name and fed to each Go kit endpoint as ClientOption.
		// In the latter case, the operation name will be the endpoint's http method.
		options = append(options, zipkin.HTTPClientTrace(zipkinTracer))
	}

	// Each individual endpoint is an http/transport.Client (which implements
	// endpoint.Endpoint) that gets wrapped with various middlewares. If you
	// made your own client library, you'd do this work there, so your server
	// could rely on a consistent set of client behavior.
	var addEventEndpoint endpoint.Endpoint
	{
		addEventEndpoint = httptransport.NewClient(
			"POST",
			copyURL(u, "/AddEvent"),
			encodeHTTPGenericRequest,
			decodeHTTPAddEventResponse,
			append(options, httptransport.ClientBefore(opentracing.ContextToHTTP(otTracer, logger)))...,
		).Endpoint()
		addEventEndpoint = opentracing.TraceClient(otTracer, "AddEvent")(addEventEndpoint)
		if zipkinTracer != nil {
			addEventEndpoint = zipkin.TraceEndpoint(zipkinTracer, "AddEvent")(addEventEndpoint)
		}
		addEventEndpoint = limiter(addEventEndpoint)
		addEventEndpoint = circuitbreaker.Gobreaker(gobreaker.NewCircuitBreaker(gobreaker.Settings{
			Name:    "AddEvent",
			Timeout: 30 * time.Second,
		}))(addEventEndpoint)
	}

	// The FetchEvents endpoint is the same thing, with slightly different
	// middlewares to demonstrate how to specialize per-endpoint.
	var fetchEventsEndpoint endpoint.Endpoint
	{
		fetchEventsEndpoint = httptransport.NewClient(
			"POST",
			copyURL(u, "/FetchEvents"),
			encodeHTTPGenericRequest,
			decodeHTTPFetchEventsResponse,
			append(options, httptransport.ClientBefore(opentracing.ContextToHTTP(otTracer, logger)))...,
		).Endpoint()
		fetchEventsEndpoint = opentracing.TraceClient(otTracer, "FetchEvents")(fetchEventsEndpoint)
		if zipkinTracer != nil {
			fetchEventsEndpoint = zipkin.TraceEndpoint(zipkinTracer, "FetchEvents")(fetchEventsEndpoint)
		}
		fetchEventsEndpoint = limiter(fetchEventsEndpoint)
		fetchEventsEndpoint = circuitbreaker.Gobreaker(gobreaker.NewCircuitBreaker(gobreaker.Settings{
			Name:    "FetchEvents",
			Timeout: 10 * time.Second,
		}))(fetchEventsEndpoint)
	}

	// Returning the endpoint.Set as a service.Service relies on the
	// endpoint.Set implementing the Service methods. That's just a simple bit
	// of glue code.
	return esendpoint.Set{
		AddEventEndpoint:    addEventEndpoint,
		FetchEventsEndpoint: fetchEventsEndpoint,
	}, nil
}

/**
 * Helper functions
 */
func copyURL(base *url.URL, path string) *url.URL {
	next := *base
	next.Path = path
	return &next
}

func errorEncoder(_ context.Context, err error, w http.ResponseWriter) {
	w.WriteHeader(err2code(err))
	_ = json.NewEncoder(w).Encode(errorWrapper{Error: err.Error()})
}

func err2code(err error) int {
	switch err {
	case service.ErrTwoZeroes, service.ErrMaxSizeExceeded, service.ErrIntOverflow:
		return http.StatusBadRequest
	}
	return http.StatusInternalServerError
}

type errorWrapper struct {
	Error string `json:"error"`
}


// Server-side parsers
func decodeHTTPAddEventRequest(_ context.Context, r *http.Request) (interface{}, error) {
	var req esendpoint.AddEventRequest
	err := json.NewDecoder(r.Body).Decode(&req)
	return req, err
}
func decodeHTTPFetchEventsRequest(_ context.Context, r *http.Request) (interface{}, error) {
	var (
		req esendpoint.FetchEventsRequest
		err error
	)
	{
		if err := r.ParseForm(); err != nil {
			return nil, err
		}

		// Stream
		req.Stream = r.Form.Get("stream")
		req.Query = r.Form.Get("query")
		{
			o := r.Form.Get("offset")
			if o != "" {
				req.Offset, err = strconv.ParseUint(o, 10, 64)
				if err != nil {
					return nil, err
				}
			} else {
				req.Offset = 0
			}

		}
		{
			l := r.Form.Get("limit")
			if l != "" {
				req.Limit, err = strconv.ParseUint(l, 10, 64)
				if err != nil {
					return nil, err
				}
			} else {
				req.Limit = 1000
			}
		}
	}

	return req, nil
}
//  Custom response encoder to return events array in stead of generic response from encodeHTTPGenericResponse
func encodeHTTPFetchEventsResponse(ctx context.Context, w http.ResponseWriter, response interface{}) error {
	if f, ok := response.(endpoint.Failer); ok && f.Failed() != nil {
		errorEncoder(ctx, f.Failed(), w)
		return nil
	}
	w.Header().Set("Content-Type", "application/json; charset=utf-8")
	return json.NewEncoder(w).Encode(response.(esendpoint.FetchEventsResponse).Events)
}

// Client-side parsers
func decodeHTTPAddEventResponse(_ context.Context, r *http.Response) (interface{}, error) {
	if r.StatusCode != http.StatusOK {
		return nil, errors.New(r.Status)
	}
	var resp esendpoint.AddEventResponse
	err := json.NewDecoder(r.Body).Decode(&resp)
	return resp, err
}
func decodeHTTPFetchEventsResponse(_ context.Context, r *http.Response) (interface{}, error) {
	if r.StatusCode != http.StatusOK {
		return nil, errors.New(r.Status)
	}
	var resp esendpoint.FetchEventsResponse
	err := json.NewDecoder(r.Body).Decode(&resp)
	return resp, err
}

// encodeHTTPGenericRequest is a transport/http.EncodeRequestFunc that
// JSON-encodes any request to the request body. Primarily useful in a client.
func encodeHTTPGenericRequest(_ context.Context, r *http.Request, request interface{}) error {
	var buf bytes.Buffer
	if err := json.NewEncoder(&buf).Encode(request); err != nil {
		return err
	}
	r.Body = ioutil.NopCloser(&buf)
	return nil
}

// encodeHTTPGenericResponse is a transport/http.EncodeResponseFunc that encodes
// the response as JSON to the response writer. Primarily useful in a server.
func encodeHTTPGenericResponse(ctx context.Context, w http.ResponseWriter, response interface{}) error {
	if f, ok := response.(endpoint.Failer); ok && f.Failed() != nil {
		errorEncoder(ctx, f.Failed(), w)
		return nil
	}
	w.Header().Set("Content-Type", "application/json; charset=utf-8")
	return json.NewEncoder(w).Encode(response)
}
