package transport

// This file contains gRPC implementation for transport
import (
	"context"
	"errors"
	"time"

	"github.com/go-kit/kit/log"

	stdopentracing "github.com/opentracing/opentracing-go"
	stdzipkin "github.com/openzipkin/zipkin-go"

	"github.com/go-kit/kit/circuitbreaker"
	"github.com/go-kit/kit/ratelimit"
	"github.com/go-kit/kit/tracing/opentracing"
	"github.com/go-kit/kit/tracing/zipkin"
	"github.com/go-kit/kit/transport"
	grpctransport "github.com/go-kit/kit/transport/grpc"
	"github.com/sony/gobreaker"

	"golang.org/x/time/rate"
	"google.golang.org/grpc"

	"github.com/go-kit/kit/endpoint"
	"gitlab.com/n13t/es/pkg/event"
	esendpoint "gitlab.com/n13t/es/pkg/eventstore/endpoint"
	"gitlab.com/n13t/es/pkg/eventstore/service"
	"gitlab.com/n13t/es/pkg/pb"
)

type grpcServer struct {
	addEvent    grpctransport.Handler
	fetchEvents grpctransport.Handler
}

// NewGRPCServer makes a set of endpoints available as a gRPC AddServer.
func NewGRPCServer(endpoints esendpoint.Set, otTracer stdopentracing.Tracer, zipkinTracer *stdzipkin.Tracer, logger log.Logger) pb.EventStoreServer {
	options := []grpctransport.ServerOption{
		grpctransport.ServerErrorHandler(transport.NewLogErrorHandler(logger)),
	}

	if zipkinTracer != nil {
		// Zipkin GRPC Server Trace can either be instantiated per gRPC method with a
		// provided operation name or a global tracing service can be instantiated
		// without an operation name and fed to each Go kit gRPC server as a
		// ServerOption.
		// In the latter case, the operation name will be the endpoint's grpc method
		// path if used in combination with the Go kit gRPC Interceptor.
		//
		// In this example, we demonstrate a global Zipkin tracing service with
		// Go kit gRPC Interceptor.
		options = append(options, zipkin.GRPCServerTrace(zipkinTracer))
	}

	return &grpcServer{
		addEvent: grpctransport.NewServer(
			endpoints.AddEventEndpoint,
			decodeGRPCAddEventRequest,
			encodeGRPCAddEventResponse,
			append(options, grpctransport.ServerBefore(opentracing.GRPCToContext(otTracer, "AddEvent", logger)))...,
		),
		fetchEvents: grpctransport.NewServer(
			endpoints.FetchEventsEndpoint,
			decodeGRPCFetchEventsRequest,
			encodeGRPCFetchEventsResponse,
			append(options, grpctransport.ServerBefore(opentracing.GRPCToContext(otTracer, "FetchEvents", logger)))...,
		),
	}
}

func (s *grpcServer) AddEvent(ctx context.Context, req *pb.AddEventRequest) (*pb.AddEventResponse, error) {
	_, rep, err := s.addEvent.ServeGRPC(ctx, req)
	if err != nil {
		return nil, err
	}
	return rep.(*pb.AddEventResponse), nil
}

func (s *grpcServer) FetchEvents(ctx context.Context, req *pb.FetchEventsRequest) (*pb.FetchEventsResponse, error) {
	_, rep, err := s.fetchEvents.ServeGRPC(ctx, req)
	if err != nil {
		return nil, err
	}
	return rep.(*pb.FetchEventsResponse), nil
}

// NewGRPCClient returns an AddService backed by a gRPC server at the other end
// of the conn. The caller is responsible for constructing the conn, and
// eventually closing the underlying transport. We bake-in certain middlewares,
// implementing the client library pattern.
func NewGRPCClient(conn *grpc.ClientConn, otTracer stdopentracing.Tracer, zipkinTracer *stdzipkin.Tracer, logger log.Logger) service.Service {
	// We construct a single ratelimiter middleware, to limit the total outgoing
	// QPS from this client to all methods on the remote instance. We also
	// construct per-endpoint circuitbreaker middlewares to demonstrate how
	// that's done, although they could easily be combined into a single breaker
	// for the entire remote instance, too.
	limiter := ratelimit.NewErroringLimiter(rate.NewLimiter(rate.Every(time.Second), 100))

	// global client middlewares
	var options []grpctransport.ClientOption

	if zipkinTracer != nil {
		// Zipkin GRPC Client Trace can either be instantiated per gRPC method with a
		// provided operation name or a global tracing client can be instantiated
		// without an operation name and fed to each Go kit client as ClientOption.
		// In the latter case, the operation name will be the endpoint's grpc method
		// path.
		//
		// In this example, we demonstrace a global tracing client.
		options = append(options, zipkin.GRPCClientTrace(zipkinTracer))

	}
	// Each individual endpoint is an grpc/transport.Client (which implements
	// endpoint.Endpoint) that gets wrapped with various middlewares. If you
	// made your own client library, you'd do this work there, so your server
	// could rely on a consistent set of client behavior.
	var addEventEndpoint endpoint.Endpoint
	{
		addEventEndpoint = grpctransport.NewClient(
			conn,
			"n13t.es.EventStore",
			"AddEvent",
			encodeGRPCAddEventRequest,
			decodeGRPCAddEventResponse,
			pb.AddEventResponse{},
			append(options, grpctransport.ClientBefore(opentracing.ContextToGRPC(otTracer, logger)))...,
		).Endpoint()
		addEventEndpoint = opentracing.TraceClient(otTracer, "AddEvent")(addEventEndpoint)
		addEventEndpoint = limiter(addEventEndpoint)
		addEventEndpoint = circuitbreaker.Gobreaker(gobreaker.NewCircuitBreaker(gobreaker.Settings{
			Name:    "AddEvent",
			Timeout: 30 * time.Second,
		}))(addEventEndpoint)
	}

	// The Concat endpoint is the same thing, with slightly different
	// middlewares to demonstrate how to specialize per-endpoint.
	var fetchEventsEndpoint endpoint.Endpoint
	{
		fetchEventsEndpoint = grpctransport.NewClient(
			conn,
			"pb.FetchEvents",
			"FetchEvents",
			encodeGRPCFetchEventsRequest,
			decodeGRPCFetchEventsResponse,
			pb.FetchEventsResponse{},
			append(options, grpctransport.ClientBefore(opentracing.ContextToGRPC(otTracer, logger)))...,
		).Endpoint()
		fetchEventsEndpoint = opentracing.TraceClient(otTracer, "FetchEvents")(fetchEventsEndpoint)
		fetchEventsEndpoint = limiter(fetchEventsEndpoint)
		fetchEventsEndpoint = circuitbreaker.Gobreaker(gobreaker.NewCircuitBreaker(gobreaker.Settings{
			Name:    "FetchEvents",
			Timeout: 10 * time.Second,
		}))(fetchEventsEndpoint)
	}

	return esendpoint.Set{
		AddEventEndpoint:    addEventEndpoint,
		FetchEventsEndpoint: fetchEventsEndpoint,
	}
}

// Server-side parser
func decodeGRPCAddEventRequest(_ context.Context, grpcReq interface{}) (interface{}, error) {
	req := grpcReq.(*pb.AddEventRequest)
	return esendpoint.AddEventRequest{Stream: req.Event.Stream, Type: req.Event.Type, Data: req.Event.Data, LatestEventID: req.LatestEventId}, nil
}

func decodeGRPCFetchEventsRequest(_ context.Context, grpcReq interface{}) (interface{}, error) {
	req := grpcReq.(*esendpoint.FetchEventsRequest)
	return esendpoint.FetchEventsRequest{Stream: req.Stream, Query: req.Query, Offset: req.Offset, Limit: req.Limit}, nil
}

func decodeGRPCAddEventResponse(_ context.Context, grpcResponse interface{}) (interface{}, error) {
	reply := grpcResponse.(*pb.AddEventResponse)
	return esendpoint.AddEventResponse{Err: str2err(reply.Err)}, nil
}

func decodeGRPCFetchEventsResponse(_ context.Context, grpcResponse interface{}) (interface{}, error) {
	reply := grpcResponse.(*pb.FetchEventsResponse)

	var events []*event.Event
	for _, pbEvent := range reply.Events {
		events = append(events, &event.Event{ID: pbEvent.Id, Type: pbEvent.Type, Data: pbEvent.Data})
	}

	return esendpoint.FetchEventsResponse{Events: events, Err: str2err(reply.Err)}, nil
}

func encodeGRPCAddEventResponse(_ context.Context, response interface{}) (interface{}, error) {
	resp := response.(esendpoint.AddEventResponse)
	return &pb.AddEventResponse{Err: err2str(resp.Err)}, nil
}

func encodeGRPCFetchEventsResponse(_ context.Context, response interface{}) (interface{}, error) {
	resp := response.(esendpoint.FetchEventsResponse)

	var pbEvents []*pb.Event
	for _, ev := range resp.Events {
		pbEvents = append(pbEvents, &pb.Event{Id: ev.ID, Stream: ev.Stream, Type: ev.Type, Data: ev.Data})
	}

	return &pb.FetchEventsResponse{Events: pbEvents, Err: err2str(resp.Err)}, nil
}

// Client-side parser
func encodeGRPCAddEventRequest(_ context.Context, request interface{}) (interface{}, error) {
	req := request.(esendpoint.AddEventRequest)
	return &pb.AddEventRequest{Event: &pb.Event{Stream: req.Stream, Type: req.Type, Data: req.Data}, LatestEventId: req.LatestEventID}, nil
}

func encodeGRPCFetchEventsRequest(_ context.Context, request interface{}) (interface{}, error) {
	req := request.(esendpoint.FetchEventsRequest)
	return &pb.FetchEventsRequest{Stream: req.Stream, Query: req.Query, Offset: req.Offset, Limit: req.Limit}, nil
}

// These annoying helper functions are required to translate Go error types to
// and from strings, which is the type we use in our IDLs to represent errors.
// There is special casing to treat empty strings as nil errors.

func str2err(s string) error {
	if s == "" {
		return nil
	}
	return errors.New(s)
}

func err2str(err error) string {
	if err == nil {
		return ""
	}
	return err.Error()
}
