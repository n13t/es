package endpoint

import (
	"context"
	"time"

	stdjwt "github.com/dgrijalva/jwt-go"
	kitjwt "github.com/go-kit/kit/auth/jwt"
	event "gitlab.com/n13t/es/pkg/event"
	eventstore "gitlab.com/n13t/es/pkg/eventstore/service"
	"gitlab.com/n13t/es/pkg/jwt"
	"golang.org/x/time/rate"

	stdopentracing "github.com/opentracing/opentracing-go"
	stdzipkin "github.com/openzipkin/zipkin-go"
	"github.com/sony/gobreaker"

	"github.com/go-kit/kit/circuitbreaker"
	"github.com/go-kit/kit/endpoint"
	"github.com/go-kit/kit/log"
	"github.com/go-kit/kit/metrics"
	"github.com/go-kit/kit/ratelimit"
	"github.com/go-kit/kit/tracing/opentracing"
	"github.com/go-kit/kit/tracing/zipkin"
)

type Set struct {
	AddEventEndpoint    endpoint.Endpoint
	FetchEventsEndpoint endpoint.Endpoint
}

// New returns a Set that wraps the provided server, and wires in all of the
// expected endpoint middlewares via the various parameters.
func New(svc eventstore.Service, jwks *jwt.Jwks, logger log.Logger, duration metrics.Histogram, otTracer stdopentracing.Tracer, zipkinTracer *stdzipkin.Tracer) Set {
	var addEventEndpoint endpoint.Endpoint
	{
		addEventEndpoint = MakeAddEventEndpoint(svc)
		if jwks != nil {
			addEventEndpoint = kitjwt.NewParser(jwks.Kf, stdjwt.SigningMethodRS256, kitjwt.MapClaimsFactory)(addEventEndpoint)
		}
		addEventEndpoint = ratelimit.NewErroringLimiter(rate.NewLimiter(rate.Every(time.Second), 1))(addEventEndpoint)
		addEventEndpoint = circuitbreaker.Gobreaker(gobreaker.NewCircuitBreaker(gobreaker.Settings{}))(addEventEndpoint)
		addEventEndpoint = opentracing.TraceServer(otTracer, "AddEvent")(addEventEndpoint)
		if zipkinTracer != nil {
			addEventEndpoint = zipkin.TraceEndpoint(zipkinTracer, "AddEvent")(addEventEndpoint)
		}
		addEventEndpoint = LoggingMiddleware(log.With(logger, "method", "AddEvent"))(addEventEndpoint)
		addEventEndpoint = InstrumentingMiddleware(duration.With("method", "AddEvent"))(addEventEndpoint)
	}
	var fetchEventsEndpoint endpoint.Endpoint
	{
		fetchEventsEndpoint = MakeFetchEventsEndpoint(svc)
		if jwks != nil {
			fetchEventsEndpoint = kitjwt.NewParser(jwks.Kf, stdjwt.SigningMethodRS256, kitjwt.MapClaimsFactory)(fetchEventsEndpoint)
		}
		fetchEventsEndpoint = ratelimit.NewErroringLimiter(rate.NewLimiter(rate.Limit(1), 100))(fetchEventsEndpoint)
		fetchEventsEndpoint = circuitbreaker.Gobreaker(gobreaker.NewCircuitBreaker(gobreaker.Settings{}))(fetchEventsEndpoint)
		fetchEventsEndpoint = opentracing.TraceServer(otTracer, "FetchEvents")(fetchEventsEndpoint)
		if zipkinTracer != nil {
			fetchEventsEndpoint = zipkin.TraceEndpoint(zipkinTracer, "FetchEvents")(fetchEventsEndpoint)
		}
		fetchEventsEndpoint = LoggingMiddleware(log.With(logger, "method", "FetchEvents"))(fetchEventsEndpoint)
		fetchEventsEndpoint = InstrumentingMiddleware(duration.With("method", "FetchEvents"))(fetchEventsEndpoint)
	}
	return Set{
		AddEventEndpoint:    addEventEndpoint,
		FetchEventsEndpoint: fetchEventsEndpoint,
	}
}

// AddEvent - This is primarily useful in the context of a client library.
func (s Set) AddEvent(ctx context.Context, Stream, Type, Data string, ProcessedEventID uint64) error {
	resp, err := s.AddEventEndpoint(ctx, AddEventRequest{Stream, Type, Data, ProcessedEventID})
	if err != nil {
		return err
	}
	response := resp.(AddEventResponse)
	return response.Err
}

// FetchEvents ...
func (s Set) FetchEvents(ctx context.Context, Stream, Type string, ProcessedEventID, Next uint64) ([]*event.Event, error) {
	resp, err := s.FetchEventsEndpoint(ctx, FetchEventsRequest{Stream, Type, ProcessedEventID, Next})
	if err != nil {
		return nil, err
	}
	response := resp.(FetchEventsResponse)
	return response.Events, response.Err
}

func MakeAddEventEndpoint(svc eventstore.Service) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		req := request.(AddEventRequest)
		err := svc.AddEvent(ctx, req.Stream, req.Type, req.Data, req.LatestEventID)

		return AddEventResponse{Err: err}, nil
	}
}

func MakeFetchEventsEndpoint(svc eventstore.Service) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		req := request.(FetchEventsRequest)
		events, err := svc.FetchEvents(ctx, req.Stream, req.Query, req.Offset, req.Limit)
		if err != nil {
			return FetchEventsResponse{nil, err}, nil
		}
		return FetchEventsResponse{events, nil}, nil
	}
}

// compile time assertions for our response types implementing endpoint.Failer.
var (
	_ endpoint.Failer = AddEventResponse{}
	_ endpoint.Failer = FetchEventsResponse{}
)

// AddEventRequest contains event properties & LastEventID for performing OCC
type AddEventRequest struct {
	Stream        string 	`json:"stream"`
	Data          string 	`json:"data"`
	Type          string	`json:"type"`
	LatestEventID uint64	`json:"latest_event_id"`
}

// AddEventResponse ONLY contains error
type AddEventResponse struct {
	Err error `json:"-"`
}

// Failed implements endpoint.Failer.
func (r AddEventResponse) Failed() error { return r.Err }

// FetchEventsRequest contains Stream, Offset, Limit
type FetchEventsRequest struct {
	Stream string `json:"stream"`
	Query  string `json:"query"`
	Offset uint64 `json:"offset"`
	Limit  uint64 `json:"limit"`
}

// FetchEventsResponse return either events of a stream or error
type FetchEventsResponse struct {
	Events []*event.Event
	Err    error `json:"-"`
}

// Failed implements endpoint.Failer.
func (r FetchEventsResponse) Failed() error { return r.Err }
