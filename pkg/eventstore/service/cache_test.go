package service

import (
	"gitlab.com/n13t/es/pkg/event"
	"testing"
)

func TestGroupCacheKeySerialize(t *testing.T) {
	tests := []struct{
		Stream, Query string
		Offset, Limit uint64
	}{
		{Stream: "my-stream", Query: "*", Offset: 0, Limit: 10000},
		{Stream: "my-stream2", Query: "*", Offset: 10, Limit: 10000},
		{Stream: "my-stream3", Query: "*", Offset: 100, Limit: 10000},
		{Stream: "my-stream4", Query: "*", Offset: 1000, Limit: 10000},
	}

	for _, k := range tests {
		s := buildGroupCacheKey(k.Stream, k.Query, k.Offset, k.Limit)
		Stream, Query, Offset, Limit, _ := parseGroupCacheKey(s)

		if k.Stream != Stream || k.Query != Query || k.Limit != Limit || k.Offset != Offset {
			t.Errorf("expected %+v have %+v\n", k, struct{Stream, Query string; Offset, Limit uint64}{Stream, Query, Offset, Limit})
		}
	}
}

func TestEventsParser(t *testing.T) {
	events := []*event.Event{
		{ID: 1, Stream: "my-stream", Data: "my-data1", Type: "plain/text"},
		{ID: 2, Stream: "my-stream", Data: "my-data2", Type: "plain/text"},
		{ID: 3, Stream: "my-stream", Data: "my-data3", Type: "plain/text"},
		{ID: 4, Stream: "my-stream", Data: "my-data4", Type: "plain/text"},
		{ID: 5, Stream: "my-stream", Data: "my-data5", Type: "plain/text"},
	}

	b, _ := eventsToBytes(events)
	actualEvents, _ := bytesToEvents(b)

	for i := 0; i < len(events); i++ {
		if events[i].ID != actualEvents[i].ID {
			t.Errorf("want %d have %d", events[i].ID, actualEvents[i].ID)
		}
		if events[i].Stream != actualEvents[i].Stream {
			t.Errorf("want %s have %s", events[i].Stream, actualEvents[i].Stream)
		}
		if events[i].Data != actualEvents[i].Data {
			t.Errorf("want %s have %s", events[i].Data, actualEvents[i].Data)
		}
		if events[i].Type != actualEvents[i].Type {
			t.Errorf("want %s have %s", events[i].Type, actualEvents[i].Type)
		}
	}
}
