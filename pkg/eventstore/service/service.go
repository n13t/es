//go:generate mockgen -source=$GOFILE -destination=mocks/$GOFILE

package service

import (
	"context"
	"errors"
	"fmt"
	"github.com/go-kit/kit/log"
	"github.com/go-kit/kit/metrics"
	"github.com/golang/groupcache"

	"gitlab.com/n13t/es/pkg/event"
)

// Service - Core Service interface
type Service interface {
	AddEvent(ctx context.Context, Stream, Type, Data string, LastestEventID uint64) error
	FetchEvents(ctx context.Context, Stream, Query string, Offset, Limit uint64) ([]*event.Event, error)
}

type service struct {
	repo      event.Repository
	grpCache  *groupcache.Group
}

func (s *service) AddEvent(ctx context.Context, Stream, Type, Data string, LatestEventID uint64) error {
	return s.repo.AddEvent(ctx, Stream, Data, event.AddEventOptions{
		ContentType: Type,
	}, LatestEventID)
}

func (s *service) FetchEvents(ctx context.Context, Stream, Query string, Offset, Limit uint64) ([]*event.Event, error) {
	return s.fetchEventsWithCache(ctx, Stream, Query, Offset, Limit, false)
}

func (s *service) fetchEventsWithCache(ctx context.Context, Stream, Query string, Offset, Limit uint64, CacheDisabled bool) ([]*event.Event, error) {

	// m, err := regexp.Compile(MatchPattern)
	// if err != nil {
	// 	return nil, errors.New(fmt.Sprintf("fetch(pattern): %v", err))
	// }

	if !CacheDisabled {
		var cachedPayload []byte
		err := s.grpCache.Get(ctx, buildGroupCacheKey(Stream, Query, Offset, Limit), groupcache.AllocatingByteSliceSink(&cachedPayload))
		if err != nil {
			// error on cache look up
			fmt.Println(err)
		} else {
			events, err := bytesToEvents(cachedPayload)
			if err != nil {
				// error on parsing cached payload
				fmt.Println(err)
			} else {
				return events, nil
			}
		}
	}

	return s.fetchEventsUsingLoop(ctx, Stream, Query, Offset, Limit)
}

func (s *service) fetchEventsUsingLoop(ctx context.Context, Stream, Query string, Offset, Limit uint64) ([]*event.Event, error) {
	events := make([]*event.Event, 0)

	// cache disabled or missed
	i := uint64(1)
	for i < Limit+1 {
		ev, err := s.repo.GetEvent(ctx, Stream, Offset+i)
		if err != nil {
			return nil, err
		}

		if ev == nil {
			break
		}

		// match event type
		// m.MatchString("")

		events = append(events, ev)
		i++
	}

	return events, nil
}

// New a Event Store Service, cacheSize is in byte unit
func New(Repo event.Repository, GroupCacheName string, CacheSize int64, logger log.Logger, ints, chars metrics.Counter) Service {
	svc := service{
		repo:     Repo,
		grpCache: groupcache.GetGroup(GroupCacheName),
	}

	if svc.grpCache == nil {
		svc.grpCache = groupcache.NewGroup(GroupCacheName, CacheSize, groupcache.GetterFunc(
			func(ctx context.Context, key string, dest groupcache.Sink) error {
				// look up cache by key
				stream, query, offset, limit, err := parseGroupCacheKey(key)
				if err != nil {
					return err
				}

				events, err := svc.fetchEventsWithCache(ctx, stream, query, offset, limit, true)
				if err != nil {
					return err
				}

				if uint64(len(events)) != limit {
					return errors.New("cache(skip response caching): not contains enough events, please try reduce limit")
				}

				b, err := eventsToBytes(events)
				if err != nil {
					return err
				}

				return dest.SetBytes(b)
			},
		))
	}

	return &svc
}
