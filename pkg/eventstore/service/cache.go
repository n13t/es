package service

import (
	"encoding/json"
	"errors"
	"fmt"
	"gitlab.com/n13t/es/pkg/event"
	"strconv"
	"strings"
)

const (
	groupCacheKeyDelimiter = ";"
)

func buildGroupCacheKey(Stream, Query string, Offset, Limit uint64) string {
	d := groupCacheKeyDelimiter
	return fmt.Sprintf("%s%s%s%s%d%s%d", Stream, d, Query, d, Offset, d, Limit)
}

func parseGroupCacheKey(Key string) (string, string, uint64, uint64, error) {
	var (
		stream, query string
		offset, limit uint64
		err error
	)


	s := strings.Split(Key, groupCacheKeyDelimiter)
	if len(s) != 4 {
		return "", "", 0, 0, errors.New("unexpected groupcache key")
	}

	if offset, err = strconv.ParseUint(s[2], 10, 64); err != nil {
		return "", "", 0, 0, errors.New("cannot read offset")
	}

	if limit, err = strconv.ParseUint(s[3], 10, 64); err != nil {
		return "", "", 0, 0, errors.New("cannot read limit")
	}

	stream, query = s[0], s[1]

	return stream, query, offset, limit, err
}

func eventsToBytes(eventPointers []*event.Event) ([]byte, error) {
	events := make([]event.Event, 0)

	for _, ep := range eventPointers {
		events = append(events, *ep)
	}

	b, err := json.Marshal(events)
	if err != nil {
		return nil, err
	}

	return b, nil
}

func bytesToEvents(data []byte) ([]*event.Event, error) {
	events := make([]event.Event, 0)
	eventPointers := make([]*event.Event, 0)


	err := json.Unmarshal(data, &events)
	if err != nil {
		return nil, err
	}

	for i, _ := range events {
		eventPointers = append(eventPointers, &events[i])
	}

	return eventPointers, nil
}
