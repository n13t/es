package service

import (
	"context"
	"testing"
	"time"

	"github.com/golang/mock/gomock"
	event "gitlab.com/n13t/es/pkg/event"
	mock_event "gitlab.com/n13t/es/pkg/event/mocks"
)

func TestFetchEvents(t *testing.T) {
	// Setup mock repo
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()

	mockEventRepo := mock_event.NewMockRepository(ctrl)

	fakeEvent := event.Event{}

	// create service using mock repo
	svc := New(mockEventRepo, "test", 64<<20, nil, nil, nil)

	// Testing
	mockEventRepo.EXPECT().GetEvent(
		gomock.Any(),
		gomock.Any(),
		gomock.Eq(uint64(1)),
	).Return(&fakeEvent, nil)
	_, _ = svc.FetchEvents(context.Background(), "fake-stream", "*", 0, 1)
}

// Run two times, expected second time will hit cache
func BenchmarkService_FetchEvents(b *testing.B) {
	ctrl := gomock.NewController(b)
	defer ctrl.Finish()

	mockEventRepo := mock_event.NewMockRepository(ctrl)

	fakeEvent := event.Event{}

	// create service using mock repo
	svc := New(mockEventRepo, "fetchEvents", 64<<20, nil, nil, nil)

	// Testing
	mockEventRepo.EXPECT().GetEvent(
		gomock.Any(),
		gomock.Any(),
		gomock.Any(),
	).Do(func(_ context.Context, _ string, _ uint64) {
		time.Sleep(5 * time.Millisecond)
	}).Return(&fakeEvent, nil).AnyTimes()

	// this will be slow
	b.Run("FirstTry", func(b *testing.B) {
		if _, err := svc.FetchEvents(context.Background(), "fake-stream", "*", 0, 1000); err != nil {
			b.Error(err)
		}
	})

	// this SHOULD hit cache
	b.Run("SecondTry", func(b *testing.B) {
		if _, err := svc.FetchEvents(context.Background(), "fake-stream", "*", 0, 1000); err != nil {
			b.Error(err)
		}
	})

}
