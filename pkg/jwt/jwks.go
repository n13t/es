package jwt

import (
	"encoding/json"
	"errors"
	"fmt"
	"net/http"

	stdjwt "github.com/dgrijalva/jwt-go"
)

const PemFormat = "-----BEGIN CERTIFICATE-----\n%s\n-----END CERTIFICATE-----"

type Jwks struct {
	Keys []struct {
		Kid     string   `json:"kid"`
		Kty     string   `json:"kty"`
		Alg     string   `json:"alg"`
		Use     string   `json:"use"`
		N       string   `json:"n"`
		E       string   `json:"e"`
		X5c     []string `json:"x5c"`
		X5t     string   `json:"x5t"`
		X5tS256 string   `json:"x5t#S256"`
	}
}

func NewCertSource(certsUrl string) (*Jwks, error) {
	resp, err := http.Get(certsUrl)
	if err != nil {
		return nil, err
	}

	defer resp.Body.Close()

	jwks := Jwks{}
	err = json.NewDecoder(resp.Body).Decode(&jwks)
	if err != nil {
		return nil, err
	}

	return &jwks, nil
}

func (ks *Jwks) getPemCert(token *stdjwt.Token) (string, error) {
	for _, k := range ks.Keys {
		if token.Header["kid"] == k.Kid {
			return fmt.Sprintf(PemFormat, k.X5c[0]), nil
		}
	}

	return "", errors.New("unable to find appropriate key")
}

func (ks *Jwks) Kf(token *stdjwt.Token) (interface{}, error) {
	// HS256
	//return []byte("your-256-bit-secret"), nil

	// RS256
	cert, err := ks.getPemCert(token)
	if err != nil {
		panic(err.Error())
	}
	result, _ := stdjwt.ParseRSAPublicKeyFromPEM([]byte(cert))
	return result, nil
}
