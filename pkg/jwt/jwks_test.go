package jwt

import (
	"crypto/rand"
	"crypto/rsa"
	"crypto/x509"
	"encoding/json"
	"encoding/pem"
	"net/http"
	"net/http/httptest"
	"testing"
	"time"

	stdjwt "github.com/dgrijalva/jwt-go"
)

func TestNewCertSource(t *testing.T) {
	jwks := Jwks{Keys: []struct {
		Kid     string   `json:"kid"`
		Kty     string   `json:"kty"`
		Alg     string   `json:"alg"`
		Use     string   `json:"use"`
		N       string   `json:"n"`
		E       string   `json:"e"`
		X5c     []string `json:"x5c"`
		X5t     string   `json:"x5t"`
		X5tS256 string   `json:"x5t#S256"`
	}{
		{Kid: "kid", Kty: "", Alg: "", Use: "", N: "", E: "", X5c: []string{}, X5t: "", X5tS256: ""},
	}}

	ts := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		b, _ := json.Marshal(jwks)
		w.WriteHeader(200)
		_, _ = w.Write(b)
	}))
	defer ts.Close()

	certSource, err := NewCertSource(ts.URL)
	if err != nil {
		t.Error(err)
	}

	if certSource.Keys[0].Kid != jwks.Keys[0].Kid {
		t.Errorf("FAILED")
	}

}

func TestJwks_Kf(t *testing.T) {
	privateKey := GeneratePrivateKey()
	publicKeyBytes, err := x509.MarshalPKIXPublicKey(&privateKey.PublicKey)
	if err != nil {
		panic(err)
	}

	publicKeyPemStr := pem.EncodeToMemory(
		&pem.Block{
			Type:  "RSA PUBLIC KEY",
			Bytes: publicKeyBytes,
		},
	)

	token := stdjwt.NewWithClaims(stdjwt.SigningMethodRS256, stdjwt.MapClaims{
		"foo": "bar",
		"nbf": time.Date(2015, 10, 10, 12, 0, 0, 0, time.UTC).Unix(),
	})
	token.Header["kid"] = "kid"

	_, err = token.SignedString(privateKey)
	if err != nil {
		t.Error(err)
	}

	jwks := Jwks{Keys: []struct {
		Kid     string   `json:"kid"`
		Kty     string   `json:"kty"`
		Alg     string   `json:"alg"`
		Use     string   `json:"use"`
		N       string   `json:"n"`
		E       string   `json:"e"`
		X5c     []string `json:"x5c"`
		X5t     string   `json:"x5t"`
		X5tS256 string   `json:"x5t#S256"`
	}{
		{Kid: "kid", Kty: "", Alg: "", Use: "", N: "", E: "", X5c: []string{string(publicKeyPemStr)}, X5t: "", X5tS256: ""},
	}}

	ts := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		b, _ := json.Marshal(jwks)
		w.WriteHeader(200)
		_, _= w.Write(b)
	}))
	defer ts.Close()

	certSource, err := NewCertSource(ts.URL)
	if err != nil {
		t.Error(err)
	}

	if certSource.Keys[0].Kid != jwks.Keys[0].Kid {
		t.Errorf("FAILED")
	}

	cert, err := certSource.Kf(token)
	if err != nil {
		t.Error("FAILED")
	}

	if cert == "" {
		t.Error("FAILED")
	}
}

func GeneratePrivateKey() *rsa.PrivateKey {
	// Private key generation
	privateKey, err := rsa.GenerateKey(rand.Reader, 2048)
	if err != nil {
		panic(err)
	}

	if err := privateKey.Validate(); err != nil {
		panic(err)
	}

	return privateKey
}
