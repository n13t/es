package errors

import "fmt"

const errorFormat = "%s(%s): %s\n%s"

var (
	ErrUnimplemented     = fmt.Errorf(errorFormat, "impl", "event", "Unimplemeted", "")
	ErrUnsupportedDriver = fmt.Errorf(errorFormat, "impl", "event", "Unsupported driver", "")
	ErrAddEventFailed    = fmt.Errorf(errorFormat, "impl", "event", "Failed to add event", "query execute successfuly but has no affected row")
)
