package main

//var eventRepository event.Repository

//var Group = groupcache.NewGroup("foobar", 64<<20, groupcache.GetterFunc(
//	func(ctx context.Context, key string, dest groupcache.Sink) error {
//		log.Println("looking up", key)
//		event, err := eventRepository.GetEvent(context.Background(), "test", 1)
//		if err != nil {
//			return err
//		}
//		// if len(events) == 0 {
//		// 	return errors.New("color not found")
//		// }
//
//		if event == nil {
//			return errors.New("event not found")
//		}
//
//		// _ = dest.SetBytes([]byte(events[0].Data))
//		_ = dest.SetBytes([]byte(event.Data))
//		return nil
//	},
//))

//func main() {
//	hostname := "localhost"
//	if os.Getenv("HOSTNAME") != "" {
//		hostname = os.Getenv("HOSTNAME")
//	}
//	selfAddr := fmt.Sprintf("http://%s:8080", hostname)
//	fmt.Println(selfAddr)
//	pool := groupcache.NewHTTPPool(selfAddr)
//
//	go func() {
//		for {
//			r := &net.Resolver{
//				PreferGo: true,
//				Dial: func(ctx context.Context, network, address string) (net.Conn, error) {
//					d := net.Dialer{
//						Timeout: 3 * time.Second,
//					}
//					return d.DialContext(ctx, network, "172.17.0.1:53")
//				},
//			}
//			names, err := r.LookupAddr(context.Background(), "hl.loc")
//			if err != nil {
//				//fmt.Println(err)
//			} else {
//				fmt.Println(names)
//			}
//			time.Sleep(3 * time.Second)
//			pool.Set("http://es1:8080", "http://es2:8080", "http://es3:8080")
//		}
//	}()
//
//	{
//		endpoint := os.Getenv("MINIO_ENDPOINT")
//		accessKeyID := "minio"
//		secretAccessKey := "minio123"
//
//		minioClient, err := minio.New(endpoint, accessKeyID, secretAccessKey, false)
//		if err != nil {
//			log.Fatalln(err)
//		}
//		eventRepository = event.NewMinIOEventRepo(minioClient, "testdata")
//	}
//
//	addr := flag.String("addr", ":8080", "server address")
//	flag.Parse()
//
//	mux := http.NewServeMux()
//	mux.HandleFunc("/events", func(w http.ResponseWriter, r *http.Request) {
//		cur := r.FormValue("current")
//
//		current, _ := strconv.ParseInt(cur, 10, 64)
//		next := int64(1000)
//
//		ctx, cancel := context.WithTimeout(context.Background(), 3*time.Second)
//		defer cancel()
//
//		fmt.Println(current, next)
//		events, err := eventRepository.Fetch(ctx, "test", current)
//		if err != nil {
//			fmt.Println(err)
//			w.WriteHeader(500)
//		} else {
//			// fmt.Println(len(events))
//			grpCache := groupcache.GetGroup("testdata")
//			if grpCache != nil {
//				fmt.Printf("%+v\n", grpCache.Stats)
//			}
//			// for i := 0; i < len(events); i++ {
//
//			// 	events[i].Data = ""
//			// }
//			b, _ := json.Marshal(events)
//			w.Write(b)
//			w.Write([]byte("\n"))
//		}
//	})
//
//	httpListener, err := net.Listen("tcp", *addr)
//	if err != nil {
//		panic(err)
//	}
//
//	registerProfile(mux)
//
//	http.Serve(httpListener, mux)
//	// http.ListenAndServe(*addr, nil)
//}


