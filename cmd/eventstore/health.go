package main

import (
	"context"
	"net/http"
)

func check(ctx context.Context) {
	//minioHealthCheck(ctx, eventRepository.)
}

func minioHealthCheck(ctx context.Context, healthEndpoint string) (string, error) {
	resp, err := http.Get(healthEndpoint)
	if err != nil {
		return "", err
	}

	if resp.StatusCode != 200 {
		return "not_serving", nil
	} else {
		return "serving", nil
	}

}
