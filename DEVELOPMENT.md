## Generate protobuffer stub

```shellscript
./script/proto-gen.sh
```

****** We use namely/protoc-all docker image to generate proto stub under the hood

## List objects in a stream by query directly to MinIO
```shell script
make -- mc config host add es http://localhost:9000 minio minio123
```

```
export MINIO_ENDPOINT=<endpoint>
make mc -- find <host>/<bucket> --name <stream>*
```

> Example: `make mc --find local/test-bucket --name 'my-stream*'`

## Update mocks files

```shellscript
make mocks
```
