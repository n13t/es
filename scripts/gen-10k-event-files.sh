#!/bin/sh
GEN_DIR=/tmp/es

mkdir -p $GEN_DIR
dd if=/dev/urandom bs=1000 count=10000 | split -a 19 -d -b 1k - $GEN_DIR/test_

if ! [ -f /tmp/mc ]; then
  wget https://dl.min.io/client/mc/release/linux-amd64/mc -O /tmp/mc
  chmod +x /tmp/mc
fi

/tmp/mc --help

#mkdir -p $GEN_DIR
#for f in {0000000..1000000}
#do
#    echo hello > "$GEN_DIR/$f.txt"
#done
