#!/bin/sh

NAMELY_PROTOC_TAG=1.29_2
LANGUAGE=go
PROTO_DIR="pkg/pb"


compile_protos() {
  echo "compile with namely/protoc-all:$NAMELY_PROTOC_TAG"
  docker run --rm -v "$PWD/$PROTO_DIR":/defs -w /defs "namely/protoc-all:$NAMELY_PROTOC_TAG" -d /defs -l $LANGUAGE -o /defs
  echo "Code generated in $PROTO_DIR/ directory"
}

compile_protos
sudo chown -R "$USER" .
